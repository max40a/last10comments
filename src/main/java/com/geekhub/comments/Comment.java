package com.geekhub.comments;

import java.time.LocalDateTime;

public class Comment {

    private LocalDateTime date;
    private String text;

    public Comment() {
    }

    public Comment(LocalDateTime date, String text) {
        this.date = date;
        this.text = text;
    }

    public LocalDateTime getDate() {
        return date;
    }

    public void setDate(LocalDateTime date) {
        this.date = date;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }


}
