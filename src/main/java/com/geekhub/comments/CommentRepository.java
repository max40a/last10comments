package com.geekhub.comments;

import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

@Repository
public class CommentRepository {

    private List<Comment> comments = new ArrayList<>();

    public void save(Comment comment) {
        comments.add(comment);
    }

    public List<Comment> find(int count) {
        return comments.stream()
                .sorted((o1, o2) -> o1.getDate().isBefore(o2.getDate()) ? 1 : o1.getDate().isAfter(o2.getDate()) ? -1 : 0)
                .limit(count)
                .collect(Collectors.toList());
    }
}
