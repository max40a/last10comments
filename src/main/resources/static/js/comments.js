function loadComments() {
    $("#post_comment").click(function () {
        $.get("/comments", function (response) {
            $("#content").empty();
            $.each(response, function (index, comment) {
                $("#content")
                    .append("<tr>")
                    .append($("<td>").text(comment.date))
                    .append($("<td>").text(comment.text))
            });
        });
    });
}

function saveComment() {
    var content = $('#comment_form').find("input[name='text']").val();
    $.post("/comments", {
        text: content
    }, "json");
}